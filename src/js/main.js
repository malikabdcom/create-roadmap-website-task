// navigation
const getHamburger = document.querySelector('[alt="hamburger"]');
const getBackdrop = document.querySelector('.backdrop');
const getMobileNav = document.querySelector('.mobile-nav-toggle');
const xButton = document.querySelector('.xbutton');

//================
// toggle nav
function toggleNav() {
  getBackdrop.classList.toggle('backdrop-toggle');
  getMobileNav.classList.toggle('mobile-nav-toggle');
}

getHamburger.addEventListener('click', toggleNav);
getBackdrop.addEventListener('click', toggleNav);
xButton.addEventListener('click', toggleNav);
