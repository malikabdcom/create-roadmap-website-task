## Quick notes before you add more features to this website.

I use scss for styling, so you will need to install sass on your local machine for compiling scss to css so the browser can read your changes.

Make sure you have already installed npm locally, then simply type

> npm install

on your terminal, it will make installation on `node_modules`'s folder.

then to compile sass and watch the file for changes, just type

> npx sass ./src/style/scss/style.scss ./src/style/css/style.css --watch

and then you are ready to go.
